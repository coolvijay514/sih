const express = require("express")
const app = express()
const axios = require("axios")
const qrcode = require("qrcode")
//app.use(bodyParser.urlencoded({ extended: true }))
app.set("view engine", "ejs")
app.use(express.static(__dirname + "/public"))

//index routes
app.get("/", (req, res) => {
	res.render("index")
})

app.get("/home", (req, res) => {
	res.render("home")
})

//Login Route
app.get("/login", (req, res) => {
	res.render("login")
})

app.post("/login", (req, res) => {
	res.redirect("/home")
})
//SHG Register Routes
app.get("/register", (req, res) => {
	res.render("register")
})

app.post("/register", (req, res) => {})

//Register Goods route
app.get("/goodsregister", (req, res) => {
	res.render("goodsregister")
})
app.post("/goodsregister", (req, res) => {
	let data = "example"

	res.render("qr", { data: data })
})

//Scan Routes
app.get("/scan", (req, res) => {
	res.render("scan")
})

app.post("/scan", (req, res) => {
	console.log(req.body.name)
})

//Port
app.listen(3000, () => {
	console.log("Server Started")
})
